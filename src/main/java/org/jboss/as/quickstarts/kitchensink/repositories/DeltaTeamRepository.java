package org.jboss.as.quickstarts.kitchensink.repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.jboss.as.quickstarts.kitchensink.model.Team;

@Repository(forEntity = Team.class)
public interface DeltaTeamRepository  extends EntityRepository<Team, Long>{
	
	Team findOptionalById(Long id);
	List<Team> findAll();
}
