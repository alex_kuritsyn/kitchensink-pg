package org.jboss.as.quickstarts.kitchensink.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.HashSet;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.as.quickstarts.kitchensink.model.Contest;
import org.jboss.as.quickstarts.kitchensink.model.Member;
import org.jboss.as.quickstarts.kitchensink.model.Team;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaTeamRepository;

@Stateless
public class TeamRegistration {

    @Inject
    private Logger log;

    @Inject
    private DeltaTeamRepository dtr;

    @Inject
    private Event<Team> memberEventSrc;

    public void save(Team team) throws Exception {
        log.info("Registering " + team.getName());
        dtr.save(team);
        memberEventSrc.fire(team);
    }

    public Boolean checkTeamEligibility(Team team) {

        if (team.getContest() == null) {
            return false;
        }

        if (team.getCoach() == null) {
            return false;
        }

        HashSet<Member> memberCounter = new HashSet<Member>(team.getMembers());
        memberCounter.add(team.getCoach());
        if (memberCounter.size() < 4)
            return false;

        for (Team t : team.getContest().getTeams()) {
            if (t.getName() == team.getName())
                return false;

            if (team.getPromotedFromTeam() == null) {
                if (memberCounter.contains(t.getCoach()))
                    return false;

                if (!Collections.disjoint(memberCounter, t.getMembers()))
                    return false;
            }
        }

        LocalDate now = LocalDate.now();
        for (Member m : team.getMembers()) {
            if (Period.between(new java.sql.Date(m.getBirthDate().getTime()).toLocalDate(), now).getYears() > 23)
                return false;
        }

        if (team.getContest().getCapacity() - team.getContest().getTeams().size() < 1)
            return false;

        return true;
    }

    public boolean checkCanEdit(Team team) {
        return team.getContest().getRegistrationAllowed();
    }

    public boolean checkCanPromote(Team team) {
        Contest toContest = team.getContest().getNextRound();
        return (toContest.getCapacity() - toContest.getTeams().size()) > 0 && team.getRank() < 6 && team.getRank() > 0;

    }
}
