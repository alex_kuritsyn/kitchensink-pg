package org.jboss.as.quickstarts.kitchensink.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.as.quickstarts.kitchensink.data.ContestListProducer;
import org.jboss.as.quickstarts.kitchensink.data.MemberListProducer;
import org.jboss.as.quickstarts.kitchensink.model.Contest;
import org.jboss.as.quickstarts.kitchensink.model.Member;
import org.jboss.as.quickstarts.kitchensink.service.ContestRegistration;

@Named
@ViewScoped
public class ContestController implements Serializable {

    public static final String BR2_MESSAGE = "BR2. team/contest changes are allowed only if contest flag indicates it is writtable";
	
    @Inject
    private FacesContext facesContext;
    
    @Inject
    private ContestRegistration contestRegistration;
    
    @Produces
    @Named
    private Contest newContest;
    
    @Inject
    MemberListProducer mlp;
    
    @Inject
    ContestListProducer clp;

    @PostConstruct
    public void initNewContest() {
        newContest = new Contest();
    }
    
    public void saveContest() throws Exception {
    	if(!contestRegistration.checkCanEdit(newContest)) {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, BR2_MESSAGE, "Registration unsuccessful");
            facesContext.addMessage(null, m);
            return;
    	}
        try {
            contestRegistration.save(newContest);
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registered!", "Registration successful");
            facesContext.addMessage(null, m);
            initNewContest();
        } catch (Exception e) {
            String errorMessage = getRootErrorMessage(e);
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Registration unsuccessful");
            facesContext.addMessage(null, m);
        }
    }
    
    public List<Member> eligibleManagers(String query) {
        List<Member> results = new ArrayList<Member>();
        for (Member member : mlp.getMembers()) {
            if(member.getName().toLowerCase().startsWith(query)) {
                results.add(member);
            }
        }
        return results;
    }
    
    public List<Contest> availableContests(String query) {
        List<Contest> results = new ArrayList<Contest>();
        for (Contest contest : clp.getContests()) {
            if(contest.getName().toLowerCase().startsWith(query)) {
                results.add(contest);
            }
        }
        return results;
    }

    public Boolean isEditable(Contest contest) {
        return contestRegistration.checkCanEdit(contest);
    }
    
    private String getRootErrorMessage(Exception e) {
        // Default to general error message that registration failed.
        String errorMessage = "Registration failed. See server log for more information";
        if (e == null) {
            // This shouldn't happen, but return the default messages
            return errorMessage;
        }

        // Start with the exception and recurse to find the root cause
        Throwable t = e;
        while (t != null) {
            // Get the message from the Throwable class instance
            errorMessage = t.getLocalizedMessage();
            t = t.getCause();
        }
        // This is the root cause message
        return errorMessage;
    }
}
