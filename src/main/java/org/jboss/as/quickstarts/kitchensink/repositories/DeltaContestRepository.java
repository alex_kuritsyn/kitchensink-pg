package org.jboss.as.quickstarts.kitchensink.repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.jboss.as.quickstarts.kitchensink.model.Contest;

@Repository(forEntity = Contest.class)
public interface DeltaContestRepository extends EntityRepository<Contest, Long> {

    Contest findOptionalById(Long id);
    List<Contest> findAll();
}
