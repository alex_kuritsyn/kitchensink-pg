package org.jboss.as.quickstarts.kitchensink.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.as.quickstarts.kitchensink.model.Team;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaTeamRepository;

@RequestScoped
public class TeamListProducer {

    @Inject
    private DeltaTeamRepository deltaRepo;

    private List<Team> teams;
    
    @Produces
    @Named
    public List<Team> getTeams() {
        return teams;
    }
    
    public void onMemberListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Team team) {
        retrieveAllTeams();
    }
    
    @PostConstruct
    public void retrieveAllTeams() {
        teams = deltaRepo.findAll();
    }
}
