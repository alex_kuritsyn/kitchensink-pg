package org.jboss.as.quickstarts.kitchensink.model;

public enum TeamState {
    ACCEPTED, 
    PENDING, 
    CANCELED
}
