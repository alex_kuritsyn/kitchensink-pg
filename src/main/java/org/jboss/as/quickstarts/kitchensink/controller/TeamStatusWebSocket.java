package org.jboss.as.quickstarts.kitchensink.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.jboss.as.quickstarts.kitchensink.controller.teamstatetopic.TeamStateTopicSender;

@ServerEndpoint("/contestDetail.xhtml")
public class TeamStatusWebSocket implements Serializable {

    @Inject
    private TeamStateTopicSender senderBean;
    
    private static List<Session> allUsers = new ArrayList<>();

    @OnMessage
    public String teamStateChange(String teamIdAndState) {
        System.out.println("Say hello to '" + teamIdAndState + "'");

        String idAndState[] = teamIdAndState.split(", ");
        senderBean.sendTeamStateUpdate(Long.valueOf(idAndState[0]), idAndState[1]);
        return ("We HaVe InFoRmEd YoUr PeErS!");
    }

    @OnOpen
    public void helloOnOpen(Session session) {
        System.out.println("WebSocket opened: " + session.getId());
        allUsers.add(session);
        session.getAsyncRemote().sendText("connected");
    }

    @OnClose
    public void helloOnClose(Session sessionToClose, CloseReason reason) {
        allUsers.remove(sessionToClose);
        System.out.println("WebSocket connection closed with CloseCode: " + reason.getCloseCode());
    }
    
    public void onJMSMessage(final @Observes MapMessage msg) {
        Logger.getLogger(TeamStatusWebSocket.class.getName()).log(Level.INFO, "Got JMS Message!");
        final String message;
        try {
            message = String.format("%d, %s", msg.getLong("team_id"), msg.getString("state"));
            allUsers.parallelStream().forEach(s -> s.getAsyncRemote().sendText(message));
        } catch (JMSException ex) {
            Logger.getLogger(TeamStatusWebSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
