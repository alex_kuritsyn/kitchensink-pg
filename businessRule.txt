BR1:
	context TeamRegistration::save(team:Team)
	pre: team.contest.capacity > team.contest.teams->size()
	and (team.members->forAll(member:Member | team.contest.teams->forAll(t:Team | t.members->excludes(member)) or team.isPromoted)
	and team.members->excludes(team.coach)
	and team.members->isUnique(member:Member | member.id)
	and team.members->forAll(member:Member | member.age < 24) 
	and team.coach != null
	and team.members->size() == 3
	and team.contest.teams->forAll(t:Team | t.name != team.name)
	 

BR2: 
	context TeamRegistration::save(team:Team)
	pre: team.contest.registrationAllowed == true

	context ContestRegistration::save(contest: Contest)
	pre: contest.registrationAllowed == true

BR3:

	context TeamRegistration::promote(team:Team)
	pre: team.contest.nextRound.capacity > team.contest.nextRound.teams->size()
	and team.rank >= 1
	and team.rank <= 5
